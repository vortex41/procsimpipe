/* 
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */


var defaultStrokeWidth = 1;
var boldStrokeWidth = 3;


function Polyline(ctx) {
	var self = this;
	self.ctx = ctx;
	self.canvas = $(ctx.canvas);
	self.ctxLeftOffset = self.canvas.offset().left;
	self.ctxTopOffset = self.canvas.offset().top;
	self.bold = false;
	self.getLineWidth = function () {
		return self.bold ? boldStrokeWidth : defaultStrokeWidth;
	};
	self.stroke = function () {
		self.ctx.stroke();
	};

	self.lastPoint = {x: 0, y: 0};
	return this;
}

Polyline.prototype = {
	constructor: Polyline,
	startFromElemHoriz: function ($elem, left, factorY, margin) {

		if (factorY === undefined) {
			factorY = 0.5;
		}
		this.ctx.stroke();
		this.ctx.beginPath();
		this.ctx.lineWidth = this.getLineWidth();
		this.lastPoint = {
			x: $elem.offset().left - this.ctxLeftOffset + (left ? +(margin ? 2 : 0) : $elem.width() - (margin ? 2 : 0)),
			y: $elem.offset().top - this.ctxTopOffset + $elem.height() * factorY
		};
		this.ctx.moveTo(this.lastPoint.x, this.lastPoint.y);
		return this;
	},
	startFromElemVert: function ($elem, top, factorX, margin) {

		if (factorX === undefined) {
			factorX = 0.5;
		}
		this.ctx.stroke();
		this.ctx.beginPath();
		this.ctx.lineWidth = this.getLineWidth();
		this.lastPoint = {
			x: $elem.offset().left - this.ctxLeftOffset + $elem.width() * factorX,
			y: $elem.offset().top - this.ctxTopOffset + (top ? +(margin ? 2 : 0) : $elem.height() - (margin ? 2 : 0))
		};
		this.ctx.moveTo(this.lastPoint.x, this.lastPoint.y);
		return this;
	},
	startFromPoint: function (x, y) {
		this.ctx.stroke();
		this.ctx.beginPath();
		this.ctx.lineWidth = this.getLineWidth();
		this.lastPoint = {
			x: x,
			y: y
		};
		this.ctx.moveTo(this.lastPoint.x, this.lastPoint.y);
		return this;
	},
	lineBy: function (deltaX, deltaY) {
		this.lastPoint = {x: this.lastPoint.x + deltaX, y: this.lastPoint.y + deltaY};
		this.lineTo(this.lastPoint.x, this.lastPoint.y);
		return this;
	},
	lineTo: function (x, y) {
		this.ctx.lineTo(x, y);
		return this;
	},
	lineHorizToElem: function ($elem, factorX) {
		if (factorX === undefined) {
			factorX = 0.5;
		}
		this.lastPoint.x = $elem.offset().left - this.ctxLeftOffset + $elem.width() * factorX;

		this.lineTo(this.lastPoint.x, this.lastPoint.y);
		return this;
	},
	lineVertToElem: function ($elem, factorY) {
		if (factorY === undefined) {
			factorY = 0.5;
		}
		this.lastPoint.y = $elem.offset().top - this.ctxTopOffset + $elem.height() * factorY;
		this.lineTo(this.lastPoint.x, this.lastPoint.y);
		return this;
	},
	stroke: function () {
		this.ctx.lineWidth = defaultStrokeWidth;//this.getLineWidth();
		this.ctx.stroke();
		return this;
	},
	setBold: function (bold) {
		this.bold = bold;
		return this;
	}
};

/**
 * 
 * @param {AsmInterpreter} asmInterpreter
 * @returns {undefined}
 */
function connectAll(asmInterpreter) {

	var canvas = document.getElementById('archConnections');
	// Make sure we don't execute when canvas isn't supported
	if (canvas.getContext) {
		var diagram = $('#architectureDiagram');
		// use getContext to use the canvas for drawing
		var ctx = canvas.getContext('2d');
		ctx.canvas.height = diagram.height();
		ctx.canvas.width = diagram.width();
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.lineWidth = defaultStrokeWidth;


		connectAluPart(ctx, asmInterpreter);
		connectPCs(ctx, asmInterpreter);
		connectIRs(ctx, asmInterpreter.freezeLevel());

		connectMemPart(ctx, asmInterpreter);

		connectElemsHoriz(ctx, $('#arch-regRES1'), $('#arch-regRES2'), true);

		connectWBpart(ctx, asmInterpreter);

	}
}

/**
 * 
 * @param {type} ctx
 * @param {AsmInterpreter} asmInterpreter
 * @returns {undefined}
 */
function connectWBpart(ctx, asmInterpreter) {


	var $regLMDR = $('#arch-regLMDR');
	var $regRES2 = $('#arch-regRES2');
	var $wbSwitch = $('#arch-switchWB');
	var polyLine = new Polyline(ctx);

	// jeśli był odczyt z pamięci, to bierzemy do WB wartość z LMDR
	var wbInstruction = asmInterpreter.IRs[3];
	/* @type PipelineFunctionalityRow */
	var wbRow = wbInstruction.row();

	var wbFromMem = wbInstruction.execPermission() && wbRow.Mem() && wbRow.Mem()[0] === 'R';
	var wbFromRes2 = wbInstruction.execPermission() && (!wbRow.Mem() || wbRow.Mem()[0] !== 'R');
	polyLine.setBold(wbFromMem)
			.startFromElemHoriz($regLMDR, false)
			.lineBy(10, 0)
			.lineVertToElem($wbSwitch, 0.66)
			.lineHorizToElem($wbSwitch, 0.99)
			.stroke();

	polyLine.setBold(wbFromRes2)
			.startFromElemVert($regRES2, false, 0.9)
			.lineVertToElem($wbSwitch, 0.33)
			.lineHorizToElem($wbSwitch, 0.99)
			.stroke();

	var $registersBox = $('#register-List');

	polyLine.setBold(wbRow.Dest() && (wbFromMem || wbFromRes2))
			.startFromElemHoriz($wbSwitch, true)
			.lineHorizToElem($registersBox, 0.9)
			.lineVertToElem($registersBox, 0.9)
			.stroke();


}

/**
 * 
 * @param {type} ctx
 * @param {AsmInterpreter} asmInterpreter
 * @returns {undefined}
 */
function connectMemPart(ctx, asmInterpreter) {
	var $regMAR = $('#arch-regMAR');
	var $regSMDR = $('#arch-regSMDR');
	var $memblock = $('#memory-block');
	var $regLMDR = $('#arch-regLMDR');
	/* @type AsmInstruction */
	var memIR = asmInterpreter.IRs[2];

	/* @type PipelineFunctionalityRow */
	var memRow = memIR.row();

	connectSquareLineRVR(ctx, $regMAR, $memblock,
			memIR.execPermission() && memRow.Mem() !== undefined,
			0.5, 0.75);
	connectSquareLineRVR(ctx, $regSMDR, $memblock,
			memIR.execPermission() && memRow.Mem() && memRow.Mem()[0] == 'W',
			0.5, 0.25);
	connectSquareLineRVR(ctx, $memblock, $regLMDR,
			memIR.execPermission() && memRow.Mem() && memRow.Mem()[0] == 'R',
			0.5, 0.5);


}

/**
 * 
 * @param {type} ctx
 * @param {AsmInterpreter} asmInterpreter
 * @returns {undefined}
 */
function connectPCs(ctx, asmInterpreter) {

	$PC = $('#arch-regPC');
	$PC1 = $('#arch-regPC1');
	connectElemsHoriz(ctx, $PC, $PC1, asmInterpreter.freezeLevel() === 0);

}

/**
 * 
 * @param {type} ctx
 * @param {AsmInterpreter} asmInterpreter
 * @returns {undefined}
 */
function connectAluPart(ctx, asmInterpreter) {

	var $alu = $('#alu');
	var $regA = $('#arch-regA');
	var $regB = $('#arch-regB');
	var $regRES1 = $('#arch-regRES1');
	var $regRES2 = $('#arch-regRES2');
	/* @type AsmInstruction */
	var aluInstruction = asmInterpreter.IRs[1];
	/* @type PipelineFunctionalityRow */
	var aluRow = aluInstruction.row();
	connectElemsHoriz(ctx, $alu, $regRES1, aluRow.ALU() && aluRow.ALU().length > 0);

	var $aluSwitch1 = $('#alu-switch1');
	var $aluSwitch2 = $('#alu-switch2');

	connectElemsHoriz(ctx, $regA, $aluSwitch1, aluInstruction.execPermission() && aluRow.S1() === 'A');
	connectElemsHoriz(ctx, $regB, $aluSwitch2, aluInstruction.execPermission() && aluRow.S2() === 'B');
	var $regPC1 = $('#arch-regPC1');
	connectSqareLineDR(ctx, $regPC1, $aluSwitch1, aluInstruction.execPermission() && aluRow.S1() === 'PC1', true, 0.5, 0.25);

	var $canvas = $(ctx.canvas);
	var ctxLeft = $canvas.offset().left;
	var ctxTop = $canvas.offset().top;

	var $regMAR = $('#arch-regMAR');
	drawSquareLineVH(ctx,
			getMidPointX($alu, $regRES1, ctxLeft),
			getMidPointY($alu, $regRES1, ctxTop),
			$regMAR.offset().left - ctxLeft + 5,
			$regMAR.offset().top - ctxTop + $regMAR.height() / 2,
			aluRow.ALU() && aluRow.ALU().length > 0);

	var $regSMDR = $('#arch-regSMDR');
	drawSquareLineVH(ctx,
			getMidPointX($regB, $aluSwitch2, ctxLeft),
			$regB.offset().top - ctxTop + $regB.height() / 2,
			$regSMDR.offset().left - ctxLeft + 5,
			$regSMDR.offset().top - ctxTop + $regSMDR.height() / 2,
			true);

	var $regIRaluPart = $('#IR1');
	connectSqareLineDR(ctx, $regIRaluPart, $aluSwitch2, aluInstruction.execPermission() && aluRow.S2() === 'IR', false, 0.5, 0.75);

	var polyLine = new Polyline(ctx);

	var cond = aluRow.JCond();
	var condSucc = false;
	if (cond && aluInstruction.execPermission()) {
		// check if the condition is about to give true
		// check only B
		var B = asmInterpreter.B.getValue();
		condSucc = asmInterpreter.pipelineFunctionalityOptions.JCond[aluRow.JCond()].apply(asmInterpreter, [B]);
	}

	var $condSwitch = $('#arch-switchCOND');

	var _x = getMidPointX($regRES1, $regRES2, ctxLeft),
			_y = $regRES1.offset().top - ctxTop + $regRES1.height() / 2;

	polyLine.setBold(condSucc)
			.startFromPoint(_x, _y)
			.lineVertToElem($condSwitch, 0.75)
			.lineHorizToElem($condSwitch, 1)
			.stroke();

	var $regsbox = $('#registers');
	_x = getMidPointX($regsbox, $regPC1, ctxLeft) - 10;
	_y = $regPC1.offset().top - ctxTop + $regPC1.height() / 2;

	polyLine.setBold(!condSucc && aluInstruction.execPermission())
			.startFromPoint(_x, _y)
			.lineVertToElem($condSwitch, 0.25)
			.lineHorizToElem($condSwitch, 1)
			.stroke();

	var $pcIncrementer = $('#PC-incrementer');
	polyLine.setBold(true)
			.startFromElemHoriz($pcIncrementer, false)
			.lineHorizToElem($condSwitch, 0)
			.stroke();

	var $PC = $('#arch-regPC');

	var ifInstruction = asmInterpreter.IRs[0];
	console.log(ifInstruction);

	var ifExec = ifInstruction && ifInstruction.execPermission() && asmInterpreter.freezeLevel() == 0;

	polyLine.setBold(ifExec)
			.startFromElemHoriz($pcIncrementer, true)
			.lineBy(-10, 0)
			.lineVertToElem($PC)
			.lineHorizToElem($PC, 0)
			.stroke();


	var ifRow = ifInstruction.row();

	// check if A should be read from registers or directly from WB value
	console.log(asmInterpreter.IRs[0]);
	var wbInstr = asmInterpreter.IRs[3];
	var ifInstr = asmInterpreter.IRs[0];
	var readAfromRegs = false;
	var readAfromWB = false;
	var readBfromRegs = false;
	var readBfromWB = false;
	var wbRow = asmInterpreter.IRs[3].row();
	var wbExec = asmInterpreter.IRs[3].execPermission();
	if (ifInstruction && ifInstruction.execPermission()) {
		var formalWB = wbRow.Dest() !== undefined ? wbInstr.formals[wbRow.Dest() - 1] : -1;
		if (ifRow.A() !== undefined) {
			readAfromRegs = true;
			if (wbExec) {
				var formalA = ifInstr.formals[ifRow.A() - 1];
				console.log(formalA);
				if (formalA == formalWB) {
					readAfromWB = true;
					readAfromRegs = false;
				}
			}
		}
		if (ifRow.B() !== undefined) {
			readBfromRegs = true;
			if (wbExec) {
				var formalB = ifInstr.formals[ifRow.B() - 1];
				if (formalB == formalWB) {
					readBfromWB = true;
					readBfromRegs = false;
				}
			}
		}
	}
	console.log(formalWB);
	console.log(ifRow.A() + ' => ' + formalA);
	console.log(ifRow.B() + ' => ' + formalB);

	var $regAswitch = $('#arch-regA-switch');
	var $regBswitch = $('#arch-regB-switch');

	var $wbSwitch = $('#arch-switchWB');

	polyLine.setBold(readAfromRegs)
			.startFromElemHoriz($regAswitch, true, 0.33)
			.lineHorizToElem($regsbox)
			.stroke();
	polyLine.setBold(readBfromRegs)
			.startFromElemHoriz($regBswitch, true, 0.33)
			.lineHorizToElem($regsbox)
			.stroke();

	polyLine.setBold(readAfromWB)
			.startFromElemHoriz($regAswitch, true, 0.66)
			.lineBy(-7, 0)
			.lineVertToElem($wbSwitch)
			.stroke();
	polyLine.setBold(readBfromWB)
			.startFromElemHoriz($regBswitch, true, 0.66)
			.lineBy(-7, 0)
			.lineVertToElem($wbSwitch)
			.stroke();

}


function getMidPointX($el1, $el2, ctxLeft) {
	return $el1.offset().left - ctxLeft + ($el2.offset().left - $el1.offset().left + $el1.width()) / 2;
}
function getMidPointY($el1, $el2, ctxTop) {
	var mid1Y = $el1.offset().top - ctxTop + $el1.height() / 2;
	var mid2Y = $el2.offset().top - ctxTop + $el2.height() / 2;
	return (mid1Y + mid2Y) / 2;
}

function connectSqareLineDR(ctx, $elem1, $elem2, bold, down, factorX, factorY) {
	var canvas = $(ctx.canvas);
	if (down === undefined) {
		down = true;
	}
	if (factorX === undefined) {
		factorX = 0.5;
	}
	if (factorY === undefined) {
		factorY = 0.5;
	}
	var ctxLeft = canvas.offset().left;
	var ctxTop = canvas.offset().top;
	if (bold === undefined) {
		bold = false;
	}
	ctx.stroke();
	ctx.beginPath();
	ctx.lineWidth = (bold ? boldStrokeWidth : defaultStrokeWidth);
	ctx.moveTo($elem1.offset().left - ctxLeft + $elem1.width() * factorX,
			$elem1.offset().top - ctxTop + (down ? $elem1.height() - 5 : 5));
	ctx.lineTo($elem1.offset().left - ctxLeft + $elem1.width() * factorX,
			$elem2.offset().top - ctxTop + $elem2.height() * factorY);
	ctx.lineTo($elem2.offset().left - ctxLeft + 5,
			$elem2.offset().top - ctxTop + $elem2.height() * factorY);
	ctx.stroke();
}

/**
 * 
 * @param {type} ctx
 * @param {jQuery} $elem1
 * @param {jQuery} $elem2
 * @param {boolean} bold
 * @returns {undefined}
 */
function connectVH(ctx, $elem1, $elem2, bold) {

	var $canvas = $(ctx.canvas);
	var ctxLeft = $canvas.offset().left;
	var ctxTop = $canvas.offset().top;
	drawSquareLineVH(ctx,
			$elem1.offset().left - ctxLeft + $elem1.width() / 2,
			$elem1.offset().top - ctxTop + 5,
			$elem2.offset().left - ctxLeft + 5,
			$elem2.offset().top - ctxTop, bold
			);
}

/**
 * Draw line from (x1,y1) to (x2,y2) first vertical then horizontal
 * @param {type} ctx
 * @param {type} x1
 * @param {type} y1
 * @param {type} x2
 * @param {type} y2
 * @param {type} bold
 * @returns {undefined}
 */
function drawSquareLineVH(ctx, x1, y1, x2, y2, bold) {
	if (bold === undefined) {
		bold = false;
	}
	ctx.stroke();
	ctx.beginPath();
	ctx.lineWidth = (bold ? boldStrokeWidth : defaultStrokeWidth);
	ctx.moveTo(x1, y1);
	ctx.lineTo(x1, y2);
	ctx.lineTo(x2, y2);
	ctx.stroke();
}

function connectSquareLineRVR(ctx, $elem1, $elem2, bold, el1Factor, el2Factor) {
	var canvas = $(ctx.canvas);
	var ctxLeft = canvas.offset().left;
	var ctxTop = canvas.offset().top;
	if (el2Factor === undefined) {
		el2Factor = 0.5;
	}
	if (el1Factor === undefined) {
		el1Factor = 0.5;
	}

	if (bold === undefined) {
		bold = false;
	}
	ctx.stroke();
	ctx.beginPath();
	ctx.lineWidth = (bold ? boldStrokeWidth : defaultStrokeWidth);
	var y1 = $elem1.offset().top - ctxTop + $elem1.height() * el1Factor;
	ctx.moveTo($elem1.offset().left - ctxLeft + $elem1.width() - 5, y1);
	var bendX = $elem1.offset().left - ctxLeft + ($elem2.offset().left + $elem1.width() - $elem1.offset().left) / 2;
	ctx.lineTo(bendX, y1);
	var y2 = $elem2.offset().top - ctxTop + $elem2.height() * el2Factor;
	ctx.lineTo(bendX, y2);
	ctx.lineTo($elem2.offset().left - ctxLeft + 5, y2);
	ctx.stroke();
}

function connectElemsHoriz(ctx, $elem1, $elem2, bold) {

	var canvas = $(ctx.canvas);
	var ctxLeft = canvas.offset().left;
	var ctxTop = canvas.offset().top;
	drawLine(ctx,
			$elem1.offset().left - ctxLeft + $elem1.width() - 5,
			$elem1.offset().top - ctxTop + $elem1.height() / 2,
			$elem2.offset().left - ctxLeft + 5,
			$elem1.offset().top - ctxTop + $elem1.height() / 2,
			bold
			);
}

function connectIRs(ctx, freezeLevel) {
	var IRs = [$('#IR0'), $('#IR1'), $('#IR2'), $('#IR3')];
	for (var i = 0; i < IRs.length - 1; i++) {
		connectElemsHoriz(ctx, IRs[i], IRs[i + 1], freezeLevel <= i + 1); // <= ?
	}
}



function drawLine(ctx, x1, y1, x2, y2, bold) {
	if (bold === undefined) {
		bold = false;
	}
	ctx.stroke();
	ctx.beginPath();
	ctx.lineWidth = (bold ? boldStrokeWidth : defaultStrokeWidth);
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.stroke();
}

function drawDot(ctx, x, y) {
// przyłącze wyjścia z rejestru do wejściowej szyny danych nr 2 (dot)
	var dotWidth = 5;
	ctx.fillRect(x - dotWidth / 2,
			y - dotWidth / 2,
			dotWidth, dotWidth);
	ctx.stroke();
}
