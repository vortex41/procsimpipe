/* 
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */


/* global functionalityOptions */


function ProcsimVM(updateAsmExecLineCb, updateUAdrLine, $textarea) {
	var self = this;

	self.$textarea = $textarea;
	self.multipleCycles = ko.observable(1);
	self.allCycles = ko.observable(0);
	self.registerList = new RegistersList(16);
	self.additionalRegistersNames = [];
	self.architectureRegisters = new ArchitectureRegisters(self.additionalRegistersNames);
	self.pipelineFunctionality = new PipelineFunctionalityTable(true);
	self.pipelineFunctionalityOpts = functionalityOptions;
	self.activityDiagramEnabled = ko.observable(true);

	self.save = function () {
		localStorage.architectureRegs = JSON.stringify(self.architectureRegisters.exportValues());
		localStorage.registerList = JSON.stringify(self.registerList.exportValues());
		self.pipelineFunctionality.saveToStorage();
	};


	self.activityDiagram = new ActivityDiagram();

	self.execLine = ko.computed(function () {
		var pc = self.architectureRegisters.getRegister('PC').register();
		updateAsmExecLineCb(pc);
		return pc;
	});

	self.architectureRegisters.additionalRegisters.subscribe(function () {
		connectAll();
	});

	self.mcAddRow = function () {
		self.pipelineFunctionality.addRow();
	};

	self.memoryMx = $('#tab-memory table').memoryMatrix(1024, 8, localStorage.ramMem ? JSON.parse(localStorage.ramMem) : null);


	self.interpreter = new AsmInterpreter(
			self.registerList,
			self.architectureRegisters,
			self.pipelineFunctionality,
			self.memoryMx,
			self.pipelineFunctionalityOpts,
			self.activityDiagram
			);
	self.currentInstructionToExec = ko.computed(function () {
		if (!self.interpreter.IRs[1].execPermission()) {
			return '';
		}

//		var row = self.pipelineFunctionality.getRow(self.interpreter.IRs[1].instruction());
		var row = self.interpreter.IRs[1].row();

		var aluOp = row ? row.ALU() : '';
		if (aluOp) {
			var s1 = row.S1();
			var s1Arg = s1;
			var s2 = row.S2();
			var s2Arg = s2;
			if (s2 === 'IR') {
				s2Arg = row.ExtIR() + '(IR)';
			}
			return aluOp + '(' + s1Arg + ',' + s2Arg + ')';
		}
		return aluOp;
	});

	self.saveToFile = function () {
		var exportData = {
			asmText: self.$textarea.val(),
			ram: self.memoryMx.export(),
			architectureRegs: self.architectureRegisters.exportValues(),
			registers: self.registerList.exportValues(),
			pipelineFunctionality: self.pipelineFunctionality.export()
		};

		var blob = new Blob([JSON.stringify(exportData)], {type: "application/json;charset=utf-8"});
		saveAs(blob, "projekt.mpr");
	};

	self.loadFromFile = function (json) {
		self.$textarea.val(json.asmText);
		self.architectureRegisters.loadFromExported(json.architectureRegs);
		self.registerList.loadFromExported(json.registers);

		self.memoryMx.loadFromJson(json.ram);
		self.pipelineFunctionality.loadFromExport(json.pipelineFunctionality);
	};

	self.handleFileSelect = function (data, event, form) {
		console.log(data);
		console.log(this);
		console.log("####################################3");
		var files = event.target.files;
		if (files.length > 0) {

			var file = files[0];
			var reader = new FileReader();
			reader.onload = function (e) {
				self.loadFromFile(JSON.parse(e.target.result));
			};
			reader.readAsBinaryString(file);
			console.log(form);
			$(form).trigger('reset');
		}
	};

	self.activityDiagramEnabled.subscribe(function () {
		self.interpreter.activityDiagramEnabled = self.activityDiagramEnabled();
		console.log(self.interpreter);
	});

	var $activityDiagramDivParent = $('#activity-diagram').parent();
	self.interpreter.activityDiagram.table.subscribe(function () {
		if (self.activityDiagramEnabled()) {
			if ($('#activity-diagram').hasClass('in')) {
//		$activityDiagramDivParent.scrollTop($activityDiagramDivParent.prop('scrollHeight')+20);
				$activityDiagramDivParent.stop().animate({scrollTop: $activityDiagramDivParent.prop('scrollHeight')});
			}
		}
	});

}
