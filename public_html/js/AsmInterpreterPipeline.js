/* 
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */



function PipeState(interpreter) {

	var self = this;
	self.interpreter = interpreter;
	self.update = function () {
		self.registerList = self.interpreter.registerList.exportValues();
		self.architectureRegisters = self.interpreter.architectureRegisters.exportValues();
	};
	this.update();
}

function parseWord(dword) {
	var f = dword & 0x0000ffff;
	if (f & 0x8000) {
		return f | 0xffff0000;
	}
	return f;
}
function parseHalf(dword) {
	return dword & 0x0000ffff;
}

function parseByte(dword) {
	return dword & 0x000000ff;
}

function parseVal(dword, part) {
	switch (part) {
		case 'Byte':
			return parseByte(dword);
		case 'Half':
			return parseHalf(dword);
		case 'Word':
			return parseWord(dword);
		default:
			return parseWord(dword);
	}
}


function AsmInstruction(dword, pipelineFunctionalityTable) {
	var self = this;
	self.wordImm = ko.observable();
	self.instruction = ko.observable();
	self.halfImm = ko.observable();
	self.byteImm = ko.observable();
	self.row = ko.observable();
	self.dword = ko.observable();
	self.execPermission = ko.observable(true);
	self.pipelineFunctionalityTable = pipelineFunctionalityTable;
	self.parseImm = function (dword, nr) {
		return (dword >> ((6 - 1 - nr) * 4)) & 0x0f;
	};
	self.destRegNr = ko.observable();
	self.set = function (dword, execPerm) {
		if (execPerm !== false) {
			execPerm = true;
		}
		self.instruction((dword & 0xff000000) >> 24);
		self.formals = [];
		var formalsCount = 6;
		for (var i = 0; i < formalsCount; i++) {
			self.formals.push(self.parseImm(dword, i));
		}
		self.wordImm(dword);
		self.halfImm(dword & 0xffff);
		self.byteImm(dword & 0xff);
		self.dword(dword);
		self.row(self.pipelineFunctionalityTable.getRow(self.instruction()));
		var d = self.row().Dest();
		self.destRegNr(d ? self.formals[d - 1] : null);
		self.execPermission(execPerm);
	};


	self.set(dword);
}



function AsmInterpreter(registerList, architectureRegisters, pipelineFunctionality, memoryMx, pipelineFunctionalityOptions, activityDiagram) {
	var self = this;
	this.registerList = registerList;
	this.activityDiagram = activityDiagram;
	this.instructionsToExec = [];
	this.architectureRegisters = architectureRegisters;
	this.labels = {};
	this.currentInstr = 0;
	this.memoryMatrix = memoryMx;
	this.pipelineFunctionality = pipelineFunctionality;
	this.uAddrReg = this.getRegisterForMc('uAR');
	this.IR = this.getRegisterForMc('IR');
	this.pipelineFunctionalityOptions = pipelineFunctionalityOptions;
	this.freezeLevel = ko.observable(0);
	this.newFetchNotExec = false;

	this.activityDiagramEnabled = true;

	this.aluOperationTypes = Object.keys(pipelineFunctionalityOptions.aluOperations);
	this.savedState = new PipeState(this);
	for (var i = 0; i < 4; i++) {
		this.IRs[i] = new AsmInstruction(0, this.pipelineFunctionality);
	}

	this.pcWb = ko.observable(false);
	self.SMDR = this.architectureRegisters.getRegister('SMDR');
	self.LMDR = this.architectureRegisters.getRegister('LMDR');
	self.RES1 = this.architectureRegisters.getRegister('RES1');
	self.RES2 = this.architectureRegisters.getRegister('RES2');
	self.B = this.architectureRegisters.getRegister('B');
	self.A = this.architectureRegisters.getRegister('A');
	self.PC = this.architectureRegisters.getRegister('PC');
	self.PC1 = this.architectureRegisters.getRegister('PC1');
}

var _startInstructionNumber = 0;
AsmInterpreter.prototype = {
	constructor: AsmInterpreter,
	/**
	 * Register with number of current pipelineFunctionality row to interpret
	 * @type RegisterAdapter
	 */
	uAddrReg: null,
	/**
	 * 
	 * @type Array
	 */
	IRs: [],
	/**
	 * 
	 * @type MemoryMatrix
	 */
	memoryMatrix: null,
	/**
	 * 
	 * @type MicrocodeTable
	 */
	pipelineFunctionality: null,
	beginCodeExecution: function () {
		this.currentInstr = 0;
//        this.getRegisterForMc('IR').setValue(this.memoryMatrix.getValue(_startInstructionNumber));
		this.getRegisterForMc('PC').setValue(_startInstructionNumber);
//		this.uAddrReg.setValue(0);
	},
	/**
	 * 
	 * @param AsmInstruction instruction
	 * @param Number nr
	 * @returns Number
	 */
	getFormalRegisterFromInstruction: function (instruction, nr) {
		console.log(instruction);
		return instruction && this.registerList.getRegister(instruction.formals[nr]);
	},
	gerFormalRegisterNumberFromInstruction: function (instruction, nr) {
		return (instruction & 0x00f00000) >> (4 * nr);
	},
// no access from asm to internal architecture registers
	isRegister: function (name) {
		return this.registerList.isRegister(name); //|| this.architectureRegisters.isRegister(name);
	},
	/**
	 * 
	 * @param {String} name
	 * @returns {Boolean}
	 */
	isRegisterForMc: function (name) {
		return this.registerList.isRegister(name) || this.architectureRegisters.isRegister(name);
	},
	/**
	 * GetRegister for pipelineFunctionality usage
	 * @param {String} regName
	 * @returns {RegisterAdapter} 
	 */
	getRegisterForMc: function (regName) {
		return this.registerList.isRegister(regName) ? this.registerList.getRegister(regName) : this.architectureRegisters.getRegister(regName);
	},
	// no access from asm to internal architecture registers
	getRegister: function (regName) {
		return this.registerList.getRegister(regName);
	},
	setInstructionsToExec: function (instructions) {
		this.instructionsToExec = [];
		this.labels = {};
		for (var i = 0; i < instructions.length; i++) {
			var instr = instructions[i].split(';')[0].trim();
			var parts = instr.split(':');
			parts[0] = parts[0].trim();
			if (parts.length === 2) {
				parts[1] = parts[1].trim();
			}
//			console.log(parts);
			if (parts.length === 1) {
				this.instructionsToExec.push(parts[0]);
			} else {
				this.instructionsToExec.push(parts[1]);
				this.labels[parts[0]] = i;
			}
		}
		this.currentInstr = 0;
		this.encodeInstructionsToMemory();
	},
	encodeInstructionsToMemory: function () {
		try {
			for (var i = 0; i < this.instructionsToExec.length; i++) {
				var instructionCode = this.encodeInstruction(this.instructionsToExec[i], i);
				this.memoryMatrix.setValue(i, instructionCode);
			}
		} catch (e) {

			console.log(e);

			alert('Błąd: ');
		}
	},
	/**
	 * 
	 * @param {String} instruction
	 * @param {Number} instructionNumber number of instruction (address in the memory where the instruction will be stored)
	 * @returns {Number} encoded instruction into 32-bit dword
	 */
	encodeInstruction: function (instruction, instructionNumber) {
		var spaceEl = instruction.indexOf(' '); // Znajdujemy pierwszą spację, po niej będą parametry
		var mnemonic = (spaceEl !== -1 ? instruction.slice(0, spaceEl) : instruction.trim());
		var mnemonicNumber = this.pipelineFunctionality.getLabelAddr(mnemonic); // [0-255]
		if (mnemonicNumber !== null) {
//            console.log(mnemonic);
			var args = spaceEl === -1 ? [] : instruction.slice(spaceEl + 1).split(',').map(function (el) {
				return el.trim();
			});
			// w args jest tablica typu: ['R1','152','R3']

			var regs = [];
			var imms = [];
			for (var i = 0; i < args.length; i++) {
				if (this.isRegister(args[i])) {
					regs.push(this.registerList.getRegisterNumber(args[i]));
				} else {
					var number = parseInt(args[i]);
					if (!isNaN(number)) {
						imms.push(number);
						// sprawdzamy czy nie jest to label
					} else if (this.labels[args[i]] !== undefined) {
//                        imms.push(this.labels[args[i]]); // bezpośredni adres do jakiej instrukcji skoczyć
						imms.push((this.labels[args[i]] - instructionNumber) * 4); // POŚREDNI adres do jakiej instrukcji skoczyć
					} else {
						throw new Error('Nieprawidłowy argument "' + args[i] + '" w linii: ' + instruction);
					}
				}
			}

			if (imms.length > 1) {
				throw new Error('Więcej niż jedna liczba podana wprost w linii ' + (instructionNumber + 1) + ': ' + instruction);
			}

			if (regs.length === 0) {
				if (imms.length === 1) {
					return (mnemonicNumber << 24) | (imms[0] & 0x00ffffff); // 24 bity na liczbę
				} else if (regs.length === 0 && imms.length === 0) {
					return (mnemonicNumber << 24); // tylko kod rozkazu na najstarszym miejscu
				}
			} else if (regs.length <= 2) {

				if (imms[0] !== undefined && imms[0] > 0x0000ffff) {
					throw new Error('Liczba ' + imms[0] + ' w instrukcji ' + instruction + ' jest zbyt duża. Musi mieścić się na 16 bitach');
				}

				return (mnemonicNumber << 24)
						| ((regs[0] << 20) | (regs[1] !== undefined ? regs[1] << 16 : 0)) // dwa rejestry
						| (imms[0] !== undefined ? imms[0] & 0x0000ffff : 0); // immediete na 16 bitach
			} else { // liczba rejestrów >2
				var ret = (mnemonicNumber << 24);
				for (var i = 0; i < regs.length; i++) {
					ret |= regs[i] << 20 - i * 4;
				}
				return ret;
			}
		} else {
			throw new Error('Niezadeklarowany mnemonik rozkazu: ' + mnemonic + ' w linii: ' + instruction);
		}
	},
	reset: function () {
		this.activityDiagram.clear();
		this.resetRegisters();
	},
	resetRegisters: function () {
		this.registerList.reset();
		this.architectureRegisters.reset();
		for (var i = 0; i < this.IRs.length; i++) {
			this.IRs[i].set(0);
		}
		this.registerList.getRegister('R0').setValue(0);
	},
	interpretNextInstruction: function () {
		this.savedState.update();
		this.freezeLevel(0);
		// executing phases from the end
		// between each pair of phases check if there is need to freeze
		if (this.IRs[3].execPermission()) {
			this.execWB();
		}
		this.IRs[3].set(this.IRs[2].dword(), this.IRs[2].execPermission());

		// ustawienie nowego rejestru PC, jeśli wykonano skok
		if (this.pcWb()) {
			this.PC.setValue(this.RES1.getValue() - 4);
			this.pcWb(false);
		}
		this.RES2.setValue(this.RES1.getValue());

		if (this.freezeAfterWB()) {
			this.IRs[3].execPermission(false);
			this.freezeLevel(4);
			return;
		}

		if (this.IRs[2].execPermission()) {
			this.execMEM();
		}
		this.IRs[2].set(this.IRs[1].dword(), this.IRs[1].execPermission());
		this.SMDR.setValue(this.B.getValue());



		if (this.freezeAfterMEM()) {
			this.IRs[2].execPermission(false);
			this.freezeLevel(3);
			this.activityDiagramEnabled && this.activityDiagram.freezeNextRow(3);
			return;
		}
		if (this.IRs[1].execPermission()) {
			this.execEX();
		}
		this.PC1.setValue(this.PC.getValue());
		this.IRs[1].set(this.IRs[0].dword(), this.IRs[0].execPermission());



		if (this.freezeAfterEX()) {
			this.IRs[1].execPermission(false);
			this.freezeLevel(2);
			this.activityDiagramEnabled && this.activityDiagram.freezeNextRow(2);
			return;
		}
		if (this.IRs[0].execPermission()) {
			this.execID();
		}


		if (this.freezeAfterID()) {
			this.freezeLevel(1);
			this.activityDiagramEnabled && this.activityDiagram.freezeNextRow(1);
			return;
		}
		this.execIF();
		if (this.activityDiagramEnabled) {
			this.activityDiagram.addInstruction(this.IRs[0].row().label());
		}
		var PC = this.architectureRegisters.getRegister('PC');
		PC.setValue(PC.getValue() + 4);
	},
	execIF: function () {
		// instruciton fetch
		var pcVal = this.architectureRegisters.getRegister('PC').getValue();
		var val = this.memoryMatrix.getDword(pcVal);
		this.IRs[0].set(val, !this.newFetchNotExec);
		this.newFetchNotExec = false;
	},
	execID: function () {

		var instruction = this.IRs[0];
		var formalA = instruction.formals[instruction.row().A() - 1];
		var formalB = instruction.formals[instruction.row().B() - 1];
		this.getRegisterForMc('A').setValue(this.registerList.getRegisterValue("R" + formalA));
		this.getRegisterForMc('B').setValue(this.registerList.getRegisterValue("R" + formalB));


	},
	execEX: function () {

		/**
		 * 
		 * @type AsmInstruction
		 */
		var instruction = this.IRs[1];
		if (instruction.row()) {
			var row = instruction.row();
			if (row.ALU()) {
				var _s1 = row.S1();
				var s1 = _s1 ? this.architectureRegisters.getRegister(_s1).getValue() : null;
				var _s2 = row.S2();
				var s2 = _s2 ? (_s2 === 'IR' ? parseVal(instruction.dword(), instruction.row().ExtIR()) :
						this.architectureRegisters.getRegister(_s2).getValue()) : null;
				var func = this.pipelineFunctionalityOptions.aluOperations[row.ALU()];
				var res = func.apply(this, [s1, s2]);
				if (res !== null) {
					this.RES1.setValue(res);
					if (row.JCond()) {
						// sprawdzamy warunek z B
//						console.log('Sprawdzam warunek!');
//						console.log(row.JCond());
						var res = this.pipelineFunctionalityOptions.JCond[row.JCond()].apply(this, [this.B.getValue()]);
						if (res) {
//							console.log('Warunek spełniony!');
							// ustawiamy przepisywanie z RES1 do PC w następnym cyklu
							this.pcWb(true);
							this.IRs[0].execPermission(false);
							this.IRs[1].execPermission(false);
							this.newFetchNotExec = true;
						}


					}
				}
			}
		} else {
			throw new Error('Cannot decode instruction with code ' + instruction.instruction());
		}
	},
	execMEM: function () {
		var instruction = this.IRs[2];
		var memInstr = instruction.row().Mem();
		if (memInstr) {
			var address = this.RES1.getValue();
			var data = this.SMDR.getValue();

//			console.log('Odpalam ' + memInstr + ' (' + address + ',' + data + ')');

			this.pipelineFunctionalityOptions.memOpts[memInstr].apply(this, [address, this.LMDR, data]);
		}
	},
	execWB: function () {

		/**
		 * 
		 * @type AsmInstruction
		 */
		var instruction = this.IRs[3];
		var dest = instruction.row().Dest();
		if (dest) {
			var value = instruction.row().Mem() ? this.LMDR.getValue() : this.RES2.getValue();
			var registerName = 'R' + instruction.destRegNr();
			this.getRegister(registerName).setValue(value);

		}
	},
	freezeAfterWB: function () {
		return false;
	},
	freezeAfterEX: function () {
		// przed wykonaniem fazy Exec

		// jeśli będzie zapis do któregoś z rejestrów, które teraz mają być odczytane, to freeze
		var instruction = this.IRs[1];

		var blockingRegNrs = [];
		// sprawdzamy S1 i S2, jeśli któryś z nich to Rxx to wrzucamy do tablicy blokowanych
		var sourceReg = instruction.row().S1();
		if (sourceReg === 'A') {
			blockingRegNrs.push(instruction.formals[instruction.row().A() - 1]);
		}
		sourceReg = instruction.row().S2();
		var mem = instruction.row().Mem();
		if (sourceReg === 'B' || instruction.row().JCond() || mem && mem[0] === 'W') {
			blockingRegNrs.push(instruction.formals[instruction.row().B() - 1]);
		}
		// jeśli któryś rejestrów docelowych (dest) w IR2 i IR3 jest w tablicy blokowanych - freeze
		var ir2dest = this.IRs[2].destRegNr();
		if (this.IRs[2].execPermission() && blockingRegNrs.indexOf(ir2dest) !== -1) {
			return true;
		}
		var ir3dest = this.IRs[3].destRegNr();
		if (this.IRs[3].execPermission() && blockingRegNrs.indexOf(ir3dest) !== -1) {
			return true;
		}


		return false;
	},
	freezeAfterMEM: function () {
		return false;
	},
	freezeAfterID: function () {
		return false;
	}
};