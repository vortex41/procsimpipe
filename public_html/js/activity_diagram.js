/* 
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */

function activityDiagramCell(instruction, frozen, colorClass) {
	var self = this;
	self.instruction = instruction;
	this.frozen = frozen;
	this.colorClass = colorClass;
}

function ActivityDiagram() {
	var self = this;
	self.table = ko.observableArray();

	self.emptyCell = new activityDiagramCell(null, false);
	self.colorClasses = ['act-diag-1', 'act-diag-2', 'act-diag-3', 'act-diag-4', 'act-diag-5'];
	self.currentColorNr = 0;

	self.clear = function () {
		self.table.removeAll();
		self.currentRow = [];
		for (var i = 0; i < 4; i++) {
			self.currentRow.push(self.emptyCell);
		}
	};
	self.clear();
	/**
	 * 
	 * @param String instruction
	 */
	self.addInstruction = function (instruction) {
		self.currentRow.pop();
		var newInstr = new activityDiagramCell(instruction, false, self.currentColorNr);
		self.currentRow.unshift(newInstr);
		self.currentColorNr = (self.currentColorNr + 1) % self.currentRow.length;
		self.table.push(self.currentRow.slice(0));
	};

	self.freezeNextRow = function (level) {
		self.currentRow.pop();
		self.currentRow.splice(level - 1, 0, self.emptyCell);
		var cl = self.currentRow.slice(0);

//		console.log(cl);
		for (var i = 0; i < level; i++) {
			cl[i] = $.extend({}, cl[i]);
			cl[i].frozen = true;
		}
		self.table.push(cl);
	};
}
