/* 
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */


ko.bindingHandlers.foreachprop = {
    transformObject: function (obj) {
        var properties = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                properties.push({ key: key, value: obj[key] });
            }
        }
        return properties;
    },
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            properties = ko.bindingHandlers.foreachprop.transformObject(value);
        ko.applyBindingsToNode(element, { foreach: properties }, bindingContext);
        return { controlsDescendantBindings: true };
    }
};

$.fn.extend({
	scrollableTableFixedHeader: function ()
	{
		var $table = $(this);
		var $ths = $('thead th', $table);
		var $elements = $($table).find('tbody tr th, tbody tr td');
		$ths.each(function (i) {
			$(this).css('width', $elements.eq(i).width() + 'px');
		});
	}

});
