/* 
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */


"use strict";

var functionalityOptions = {
	aluOperations: {
		NOP: function () {
			return null;
		},
		ADD: function (S1, S2) {
			return S1 + S2;
		},
		SUB: function (S1, S2) {
			return S1 - S2;
		},
		RSUB: function (S1, S2) {
			return S2 - S1;
		},
		AND: function (S1, S2) {
			return S1 & S2;
		},
		OR: function (S1, S2) {
			return S1 | S2;
		},
		XOR: function (S1, S2) {
			return S1 ^ S2;
		},
		S1: function (S1, S2) {
			return S1;
		},
		S2: function (S1, S2) {
			return S2;
		},
		S1S2: function (S1, S2) {
			return ((S1 & 0x0000ffff) << 16) | (S2 & 0x0000ffff);
		},
		INCS1: function (S1, S2) {
			return S1 + 1;
		},
		INCS2: function (S1, S2) {
			return S2 + 1;
		},
		MUL: function (S1, S2) {
			return S1 * S2;
		},
		DIV: function (S1, S2) {
			return S1 / S2;
		},
		RDIV: function (S1, S2) {
			return S2 / S1;
		},
	},
	s1opts: ['A', 'PC1'],
	s2opts: ['B', 'IR'],
	destOpts: ['1', '2', '3'],
	extIrOpts: ['Byte', 'Half', 'Word'],
	JCond: {
		// ten obiekt zostanie wstrzyknięty do interpretera mikrokodu
		// w funkcjach pod this zostanie podstawiony AsmInterpreter
		True: function () {
			return true;
		},
		EQ: function (result) {
			return result === 0;
		},
		NE: function (result) {
			return result !== 0;
		},
		LT: function (result) {
			return result < 0;
		},
		LE: function (result) {
			return result <= 0;
		},
		GT: function (result) {
			return result > 0;
		},
		GE: function (result) {
			return result >= 0;
		}
	},
	// Sprawdzić zapis pamięci
	memOpts: {
		/**
		 * 
		 * @param {Number} address
		 * @param {RegisterAdapter} destReg
		 * @param {Number} regValue value of register from last cycle
		 * @returns {undefined}
		 */
		RB: function (address, destReg, regValue) {
			var mask = 0x000000ff << 8 * (address % 4);
			var value = this.memoryMatrix.getValue(address / 4) & mask;

			destReg.setValue((regValue & 0xffffff00) | value);
			return value;
		},
		RH: function (address, destReg, regValue) {
			var value = this.memoryMatrix.getWord(address);
			destReg.setValue((regValue & 0xffff0000) | value);
			return value;

		},
		RW: function (address, destReg, regValue) {
			var value = this.memoryMatrix.getDword(address);
			destReg.setValue(value);
			return value;
		},
		WB: function (address, destReg, regValue) {
//			var value = regValue & 0x000000ff;
//			var memValue = this.memoryMatrix.getValue(address);
//			var newValue = (memValue & 0xffffff00) | value;

//			this.memoryMatrix.setValue(address, newValue);
			this.memoryMatrix.setByte(address, regValue);
			return regValue;
		},
		WH: function (address, destReg, regValue) {
//			var value = regValue & 0x000ffff;
//			var memValue = this.memoryMatrix.getValue(address);
//			var newValue = (memValue & 0xffff0000) | value;
//			this.memoryMatrix.setValue(address, newValue);
//			return value;

			this.memoryMatrix.setHalf(address, regValue);
			return regValue;
		},
		WW: function (address, destReg, regValue) {
//			this.memoryMatrix.setValue(address, regValue);
			this.memoryMatrix.setWord(address, regValue);
			return regValue;
		}
	},
	A: {
		'1': function () {
			var regA = this.architectureRegisters.getRegister('A');
			var IrValue = this.IRs[1];
			var r1Nr = (IrValue & 0x00f00000) >> 20;
			regA.setValue(this.getRegister(r1Nr));
		},
		'2': function () {
			var regA = this.architectureRegisters.getRegister('A');
			var IrValue = this.IRs[1];
			var r1Nr = (IrValue & 0x000f0000) >> 16;
			regA.setValue(this.getRegister(r1Nr));
		},
		'3': function () {
			var regA = this.architectureRegisters.getRegister('A');
			var IrValue = this.IRs[1];
			var r1Nr = (IrValue & 0x0000f000) >> 12;
			regA.setValue(this.getRegister(r1Nr));
		}
	},
	B: {
		'1': function () {
			var regB = this.architectureRegisters.getRegister('B');
			var IrValue = this.IRs[1];
			var r1Nr = (IrValue & 0x00f00000) >> 20;
			regB.setValue(this.getRegister(r1Nr));
		},
		'2': function () {
			var regB = this.architectureRegisters.getRegister('B');
			var IrValue = this.IRs[1];
			var r1Nr = (IrValue & 0x000f0000) >> 16;
			regB.setValue(this.getRegister(r1Nr));
		},
		'3': function () {
			var regB = this.architectureRegisters.getRegister('B');
			var IrValue = this.IRs[1];
			var r1Nr = (IrValue & 0x0000f000) >> 12;
			regB.setValue(this.getRegister(r1Nr));
		}
	}
};

// PipelineFunctionalityRow exported properties
var _functionalityRowProperties = ['label', 'A', 'B', 'Dest', 'S1', 'S2', 'ExtIR', 'ALU', 'JCond', 'Mem'];


function PipelineFunctionalityRow() {
	var self = this;
	self.label = ko.observable();
	self.A = ko.observable();
	self.B = ko.observable();
	self.Dest = ko.observable();
	self.S1 = ko.observable();
	self.S2 = ko.observable();
	self.ExtIR = ko.observable();
	self.ALU = ko.observable();
	self.JCond = ko.observable();
	self.Mem = ko.observable();

	self.export = function () {
		var ret = {};
		for (var i = 0; i < _functionalityRowProperties.length; i++) {
			var property = _functionalityRowProperties[i];
			ret[property] = this[property]();
		}
		return ret;
	};

	self.loadFromExport = function (dataObj) {

		for (var i = 0; i < _functionalityRowProperties.length; i++) {
			var property = _functionalityRowProperties[i];
			if (dataObj[property]) {
				this[property](dataObj[property]);
			} else {
				this[property](null);
			}
		}
	};

}



var defaultMicrocodeSize = 50;

/**
 * 
 * @param {Boolean} loadFromStorage if content should be loaded from localStorage
 * @returns {PipelineFunctionalityTable}
 */
function PipelineFunctionalityTable(loadFromStorage) {
	var self = this;

	this.functionalityObsArr = ko.observableArray(
			Array.apply(null, {length: defaultMicrocodeSize}).map(
			function () {
				return new PipelineFunctionalityRow();
			}
	));

	/**
	 * Returns uAddr of row in microcode table
	 * @param {String} label label
	 * @returns {Number} >=0 when found or -1 otherwise
	 */
	this.getLabelAddr = function (label) {

		// TODO better performance
		var arr = this.functionalityObsArr();
		for (var i = 0; i < arr.length; i++) {
			if (arr[i].label() === label) {
				return i;
			}
		}
		return null;
	};



	/**
	 * 
	 * @param {Number} uAddr PipelineFunctionalityRow address
	 * @returns {PipelineFunctionalityRow} PipelineFunctionalityRow at uAddr in uCode table
	 */
	this.getRow = function (uAddr) {
		if (uAddr >= this.functionalityObsArr().length || uAddr < 0) {
			throw new Error("Invalid address of Opcode!");
		} else {
			return this.functionalityObsArr()[uAddr];
		}
	};



	/**
	 * Loads data from localStorage
	 * @returns {Boolean} if loading was successful
	 */
	this.loadFromStorage = function () {
		if (localStorage.microcode !== undefined) {
			var json = JSON.parse(localStorage.microcode);
			this.loadFromExport(json);
			return true;
		}
		return false;
	};

	this.loadFromExport = function (json) {
		var microcodeArray = self.functionalityObsArr();
		var i = 0;
		for (; i < json.length; i++) {
			if (microcodeArray[i]) {
				microcodeArray[i].loadFromExport(json[i]);
			} else {
				var row = new PipelineFunctionalityRow(i);
				row.loadFromExport(json[i]);
				self.functionalityObsArr.push(row);
			}
		}
		while (i < self.functionalityObsArr().length) {
			self.functionalityObsArr.pop();
		}
	};

	this.saveToStorage = function () {
		localStorage.microcode = JSON.stringify(this.export());
	};

	this.export = function () {
		return this.functionalityObsArr()
				.map(function (element, i) {
					return element.export();
				});
	};

	this.removeRow = function (mcRow) {
		self.functionalityObsArr.remove(mcRow);
	};

	this.addRow = function () {
		self.functionalityObsArr.push(new PipelineFunctionalityRow());
	};


	if (loadFromStorage) {
		this.loadFromStorage();
	}

}

